package algorithms.gzip;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class GZipAlgorithmTest {

    String str = "Test text fo checking the algorithm";
    String compressedData = GZipAlgorithm.getInstance().compress(str);
    String decompressedData = GZipAlgorithm.getInstance().decompress(compressedData);

    @Test
    void compress() {
        assertNotEquals(str, compressedData);
    }

    @Test
    void decompress() {
        assertEquals(str, decompressedData);
    }
}