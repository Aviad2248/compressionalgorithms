package algorithms.lzw;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class LZWAlgorithmTest {

    String str = "Test text fo checking the algorithm";
    String compressedData = LZWAlgorithm.getInstance().compress(str);
    String decompressedData = LZWAlgorithm.getInstance().decompress(compressedData);

    @Test
    void compress() {
        assertNotEquals(str, compressedData);
    }

    @Test
    void decompress() {
        assertEquals(str, decompressedData);
    }
}