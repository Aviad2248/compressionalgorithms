package algorithms.gzip;

import algorithms.IAlgoCompression;
import json.JsonManager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Author: Shahar Azar
 * Date:   20/01/2022
 * Purpose: A class to implement the GZip algorithm
 **/
public class GZipAlgorithm implements IAlgoCompression {
    private static GZipAlgorithm instance = null;
    private final JsonManager jsonManager = JsonManager.getInstance();

    private GZipAlgorithm() {
    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static GZipAlgorithm getInstance() {
        if (instance == null) {
            instance = new GZipAlgorithm();
        }
        return instance;
    }

    @Override
    public String compress(String str) {
        if (str == null) {
            return "";
        }

        if (str.length() == 0) {
            return jsonManager.toJson(str.getBytes(), byte[].class);
        }

        try {
            ByteArrayOutputStream obj = new ByteArrayOutputStream();
            GZIPOutputStream gzip = new GZIPOutputStream(obj);
            gzip.write(str.getBytes(StandardCharsets.UTF_8));
            gzip.close();
            return jsonManager.toJson(obj.toByteArray(), byte[].class);
        } catch (Exception e) {
            return jsonManager.toJson(str.getBytes(), byte[].class);
        }
    }

    @Override
    public String decompress(String str) {
        if (str == null) {
            return "";
        }

        if (str.length() == 0) {
            return jsonManager.toJson(str.getBytes(), byte[].class);
        }

        try {
            GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(jsonManager.fromJson(str, byte[].class)));
            BufferedReader bf = new BufferedReader(new InputStreamReader(gis, StandardCharsets.UTF_8));
            StringBuilder outStr = new StringBuilder();
            String line;
            while ((line = bf.readLine()) != null) {
                outStr.append(line);
            }
            return outStr.toString();
        } catch (Exception e) {
            return jsonManager.toJson(str.getBytes(), byte[].class);
        }
    }
}
