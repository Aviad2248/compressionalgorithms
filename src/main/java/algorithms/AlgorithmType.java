package algorithms;

/**
 * Author: Aviad Partoosh
 * Date:   01/12/2021
 * Purpose: An enum that represents all of the algorithm we have
 **/

public enum AlgorithmType {

    NONE,
    LZW,
    GZIP,

}
