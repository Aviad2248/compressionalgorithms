package algorithms;

/**
 * Author: Aviad Partoosh
 * Date:   16/11/2021
 * Purpose: Functions that every algorithms has to have.
 **/
public interface IAlgoCompression {

    String compress(String str);

    String decompress(String str);

}
